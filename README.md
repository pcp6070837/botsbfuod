# BotsBFUOD

Detecting Web Bots using Outlier detection on biometric features.

Read thesis.pdf for more.

## Project strutre

`code/bots/`

    is a collection of bots developped to test the solution

`code/collector`

    is the JS that collects biometric data

`code/detector`

    the ML model and experiments