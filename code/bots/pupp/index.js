const puppeteer = require("puppeteer");

function delay(time) {
    return new Promise(function (resolve) {
        setTimeout(resolve, time);
    });
}

// handles the browser
let browser = null;
(async () => {
    // Launch a browser and turn the headless off so that you can see what is going on
    browser = await puppeteer.launch({
        headless: false,
        defaultViewport: null,
        width: 1600,
        height: 940,
    });
    // open a new tab in the browser

    const context = browser.defaultBrowserContext();

    let page = await context.newPage();
    await page.setViewport({ width: 1366, height: 768})
    await page.setUserAgent("puppet");
    // set device size to stick to only desktop view

    // open a URL
    page.goto("https://jscrambler.com/", {
        waitUntil: "domcontentloaded",
    });

  

    await page.mouse.move(200, 100)
    await page.waitForNavigation();
    await page.click("#products");
    // await page.evaluate(() => {
    //     const element = document.createElement("div");
    //     element.id = "mymouse";
    //     document.body.appendChild(element);
    //     onmousemove = (e) => {
    //         element.innerHTML = "" + e.clientX + ":" + e.clientY;
    //         console.log(element.innerHTML);
    //     };
    // });

    // await Promise.all( [
    //   page.mouse.move(1500, 1500, { steps: 256 }),
    //   page.mouse.click(),
    //   delay(1000),
    //   page.waitForNavigation()
    // ]);
    //https://
    // await page.evaluate(() =>{
    //   elem = document.getElementById("team");
    //   elem.scrollIntoView({behavior: "smooth", block: "end"});
    // });

    /*
      Anotating Positions on screen for jscrambler stuff
    */
    // wait for the search input to have finished loading on the page
    // await page.waitForSelector('input[name="search-field"]');
    // // Delay 2seconds before typing
    // await delay(2000);
    // // target the search input and type into the field with a little delay so you can see whats going on
    // await page.type('input[name="search-field"]', "Obudu Cattle Ranch, Obudu, Nigeria", {
    //     delay: 5,
    //   });
    // // target and click the search button
    // await page.click('input[name="submit-button"]');
    // wait 5 seconds



    //https://www.delftstack.com/howto/javascript/get-position-of-element-in-javascript/#use-the-element.getboundingclientrect-function-to-get-the-position-of-an-element-in-javascript 
    //to get elem pos

    await delay(5000);
    // close the browser
    await browser.close();
})().catch((error) => {
    console.log(error);
});
