import time
from selenium import webdriver

from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By	


driver = webdriver.Firefox()

driver.get("http://localhost/capazpsicologia/marcacoes/")

nameInput = WebDriverWait(driver, 3).until(lambda d: d.find_element(By.ID, "name"))
emailInput = driver.find_element(By.ID, "email")
descInput =  driver.find_element(By.TAG_NAME, "textarea")

nameInput.send_keys("Rogério Rogério")
emailInput.send_keys("rr@gmail.com")
descInput.send_keys("Test")

btn = driver.find_element(By.ID, "sub_btn")
btn.click()

time.sleep(2)
    

driver.quit()



