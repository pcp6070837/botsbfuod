import time
from selenium import webdriver

from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys

url = "https://jscrambler.pt"


options = webdriver.FirefoxOptions()
options.set_preference("general.useragent.override", "RavioliFunghiBot")

driver = webdriver.Firefox(options=options)
driver.get(url)

actions = ActionChains(driver)

cookies =  WebDriverWait(driver, 3).until(lambda d: d.find_element(By.ID, "CybotCookiebotDialogBodyLevelButtonLevelOptinAllowAll"))
products = WebDriverWait(driver, 3).until(lambda d: d.find_element(By.ID, "products"))

productsToView = []


actions.move_to_element(cookies)
actions.click()
actions.perform()
actions.reset_actions()

actions.move_to_element(products)
actions.perform()
actions.reset_actions()


productLinks = WebDriverWait(driver, 3).until(lambda d: d.find_elements(By.CLASS_NAME, "linkWrapper--7sh4o"))
for p in productLinks:
	span = WebDriverWait(driver, 3).until(lambda d: p.find_element(By.TAG_NAME, "span"))
	productsToView.append(span.text)

for p in productsToView:
	link = WebDriverWait(driver, 10).until(lambda d: d.find_element(By.XPATH, "//*[contains(text(),'" + p + "')]"))
	actions.move_to_element(link)
	actions.click()
	actions.perform()
	actions.reset_actions()
	time.sleep(1)
	footer = WebDriverWait(driver, 3).until(lambda d: d.find_element(By.TAG_NAME, "footer"))
	driver.execute_script("arguments[0].scrollIntoView()", footer)
	
	driver.back()
	products = WebDriverWait(driver, 3).until(lambda d: d.find_element(By.ID, "products"))
	actions.move_to_element(products)
	actions.perform()
	actions.reset_actions()
	time.sleep(1)

res = WebDriverWait(driver, 3).until(lambda d: d.find_element(By.ID, "resources"))
actions.move_to_element(res)
actions.click()
actions.perform()
actions.reset_actions()
footer = WebDriverWait(driver, 3).until(lambda d: d.find_element(By.TAG_NAME, "footer"))
driver.execute_script("arguments[0].scrollIntoView()", footer)
time.sleep(1)

par = WebDriverWait(driver, 3).until(lambda d: d.find_element(By.ID, "partners"))
actions.move_to_element(par)
actions.click()
actions.perform()
actions.reset_actions()
footer = WebDriverWait(driver, 3).until(lambda d: d.find_element(By.TAG_NAME, "footer"))
driver.execute_script("arguments[0].scrollIntoView()", footer)
time.sleep(1)

time.sleep(3)
driver.quit()