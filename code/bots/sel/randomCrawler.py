import time
from selenium import webdriver

from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By	

visited = []
urls = ["https://jscrambler.pt"]


options = webdriver.FirefoxOptions()
options.set_preference("general.useragent.override", "crawler0")

driver = webdriver.Firefox(options=options)

links = []

while len(urls) > 0:
	url = urls.pop(0)
	driver.get(url)
	visited.append(url)
	links = WebDriverWait(driver, 15).until(lambda d: d.find_elements(By.TAG_NAME, "a"))
	for link in links:
		try:
			nurl = link.get_attribute("href")
			if nurl and nurl not in visited and nurl not in urls and nurl.find("jscrambler.") != -1 and nurl.find("http") == 0:
				urls.append(nurl)
				print(nurl)
		except Exception:
			continue


time.sleep(2)

print(len(visited))
    
driver.quit()
