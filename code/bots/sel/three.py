import time
import random

import windmouse

from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys

options = webdriver.FirefoxOptions()
options.set_preference("general.useragent.override", "SpagetBot")

driver = webdriver.Firefox(options=options)

path = []
mouse = 0
scroll = 0
actions = ActionChains(driver, duration=25)

def mimicHumanType(chain, str):
	for char in str:
		ttw = random.randrange(1, 100)/100 #need to tune this probably
		chain.pause(ttw)
		chain.send_keys(char)

def mimicHumanPath(xoffset, yoffset):
	if xoffset < 0: xoffset = 0

	if xoffset > int(driver.get_window_size().get("width")): xoffset = int(driver.get_window_size().get("width")) - 1

	if yoffset < 0: yoffset = 0

	if yoffset >  int(driver.get_window_size().get("height")): yoffset = int(driver.get_window_size().get("height")) - 1

	path.append([xoffset, yoffset])

def mimicHumanMouseMove(chain, x_to, y_to):
	path.clear()
	mouX = 0
	mouY = 0
	try:
		(mouX, mouY) = getMouse()
	except Exception:
		injectMouse()
		(mouX, mouY) = getMouse()
	windmouse.windmouse(mouX, mouY, x_to, y_to, move_mouse=mimicHumanPath)
	# print(mouX)
	# print(mouY)
	for point in path:
		#print("In loop")
		offX = point[0] - mouX
		if mouX + offX < 0:
			offX = 0
		offY = point[1] - mouY
		if mouY + offY < 0:
			offY = 0
		# print(str(mouX) + ":" + str(mouY))
		# print(str(offX) + ":" + str(offY))
		chain.move_by_offset(offX, offY)
		(mouX, mouY) = point

def mimicHumanMouseMoveToElem(element):
	path.clear()
	x_to = int(element.location.get("x"))
	y_to = int(element.location.get("y"))

	# if x_to == 0: x_to = 20
	# if x_to >= int(driver.get_window_size().get("width")): x_to = int(driver.get_window_size().get("width")) - 20

	# if y_to == 0: y_to = 20
	# if y_to >= int(driver.get_window_size().get("height")): y_to = int(driver.get_window_size().get("height")) - 20
	print(element.rect)
	print("Where to: " + str(x_to) + ":" + str(y_to))
	mimicHumanMouseMove(actions, x_to, y_to)
	actions.move_to_element(element)

# def setMouse(x, y):
# 	driver.execute_script("""
# 	const element = document.getElementById()
# 	""")

# To use the windmouse code, we need the current mouse position, so we inject this hacky div to keep it
# https://stackoverflow.com/questions/15510882/selenium-get-coordinates-or-dimensions-of-element-with-python -Z this gets the coords to where we wanna go
def injectMouse():
	driver.execute_script("""
			const element = document.createElement('div');
			element.id = "mymouse"
			document.body.appendChild(element);
			onmousemove = (e) => { 
				element.innerHTML = "" + e.clientX + ":" + e.clientY;
				console.log(element.innerHTML);
			};
			onclick = (e) => {
				console.log("AAAA" + e.clientX + ":" + e.clientY);
			};
		""")
	act = ActionChains(driver)
	act.move_by_offset(driver.get_window_size().get("width")/2, driver.get_window_size().get("height")/2)
	act.perform()
	del act

def getMouse():
	mouse = WebDriverWait(driver, 3).until(lambda d: d.find_element(By.ID, "mymouse"))
	return (int(mouse.text.split(":")[0]), int(mouse.text.split(":")[1]))

def injectScroll():
	driver.execute_script("""
		const scrollElem = document.createElement('div');
		scrollElem.id = "myscroll";
		scrollElem.innerHTML = "" + document.body.scrollTop + ":" + document.body.scrollLeft;
		document.body.appendChild(scrollElem);
		onscroll = (e) => { scrollElem.innerHTML = "" + document.body.scrollTop + ":" + document.body.scrollLeft;}
	""")

def getScroll():
	scroll = WebDriverWait(driver, 3).until(lambda d: d.find_element(By.ID, "myscroll"))
	return (int(scroll.text.split(":")[0]), int(scroll.text.split(":")[1]))

def microScroll(x, y):
	driver.execute_script("""scrollTo({
	top:""" + str(y) + """, 
	left:""" + str(x) + """, 
	behavior: 'smooth' 
	})""")

def mimicHumanScroll(element):
	x_to = int(element.location.get("x"))
	y_to = int(element.location.get("y"))
	x = 0
	y = 0
	try:
		(x, y) = getScroll()
	except Exception:
		injectScroll()
		(x, y) = getScroll()

	while x != x_to or y != y_to:
		mult_x = 0
		mult_y = 0
		add_x = 0
		add_y = 0
		if  abs(x_to - x) > 400:
			add_x = random.randrange(150, 400)
		else:
			add_x = random.randrange(2, 20)
		
		if  abs(y_to - y) > 400:
			add_y = random.randrange(150, 400)
		else:
			add_y = random.randrange(2, 20)
		if x < x_to:
			mult_x = 1
		if x > x_to:
			mult_x = -1
		if y < y_to:
			mult_y = 1
		if y > y_to:
			mult_y = -1

		if abs(x_to - x) < add_x:
			add_x = abs(x_to - x)

		if abs(y_to - y) < add_y:
			add_y = abs(y_to - y)

		x = x + (mult_x*add_x)
		y = y + (mult_y*add_y)
		microScroll(x, y)
		time.sleep(random.randrange(0, 3)/15)