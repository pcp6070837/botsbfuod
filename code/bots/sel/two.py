import time
from selenium import webdriver

from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys


driver = webdriver.Firefox()

driver.get("http://localhost/capazpsicologia/marcacoes/")

actions = ActionChains(driver)

nameInput = WebDriverWait(driver, 3).until(lambda d: d.find_element(By.ID, "name"))
emailInput = WebDriverWait(driver, 3).until(lambda d: d.find_element(By.ID, "email"))
descInput =  WebDriverWait(driver, 3).until(lambda d: d.find_element(By.TAG_NAME, "textarea"))
subBtn = WebDriverWait(driver, 3).until(lambda d: d.find_element(By.ID, "sub_btn"))

actions.move_to_element(nameInput)
actions.click()
actions.move_to_element(emailInput)
actions.click()
actions.perform()

time.sleep(2)

driver.quit()
