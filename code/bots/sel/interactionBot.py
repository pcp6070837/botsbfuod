import three
from selenium.common.exceptions import MoveTargetOutOfBoundsException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

from selenium.webdriver import ActionChains

import time

url = "https://jscrambler.pt"

three.driver.get(url)

print("Accepting cookies")
cookies =  WebDriverWait(three.driver, 3).until(lambda d: d.find_element(By.ID, "CybotCookiebotDialogBodyLevelButtonLevelOptinAllowAll"))
three.mimicHumanMouseMoveToElem(cookies)
three.actions.click()
three.actions.perform()	
three.actions.reset_actions()

print("Move to Solutions, hover")
sols = WebDriverWait(three.driver, 3).until(lambda d: d.find_element(By.ID, "solutions"))

(mX, mY) = three.getMouse()
three.actions.move_by_offset(mX, mY)
three.mimicHumanMouseMoveToElem(sols)
three.actions.perform()
time.sleep(1.5)
three.actions.reset_actions()

print("Move to Products, hover")
products = WebDriverWait(three.driver, 3).until(lambda d: d.find_element(By.ID, "products"))

(mX, mY) = three.getMouse()
three.actions.move_by_offset(mX, mY)
three.mimicHumanMouseMoveToElem(products)
three.actions.perform()
time.sleep(1.5)
three.actions.reset_actions()

print("Move to Resources, hover")
res = WebDriverWait(three.driver, 3).until(lambda d: d.find_element(By.ID, "resources"))

(mX, mY) = three.getMouse()
three.actions.move_by_offset(mX, mY)
three.mimicHumanMouseMoveToElem(res)
three.actions.perform()
time.sleep(1.5)
three.actions.reset_actions()

print("Exiting")

three.driver.quit()
