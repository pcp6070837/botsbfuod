import three
from selenium.common.exceptions import MoveTargetOutOfBoundsException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

from selenium.webdriver import ActionChains

import time

url = "https://jscrambler.pt"

three.driver.get(url)

print("Accepting cookies")
cookies =  WebDriverWait(three.driver, 3).until(lambda d: d.find_element(By.ID, "CybotCookiebotDialogBodyLevelButtonLevelOptinAllowAll"))
three.mimicHumanMouseMoveToElem(cookies)
three.actions.click()
three.actions.perform()	
three.actions.reset_actions()


print("Move to Products, hover")
products = WebDriverWait(three.driver, 3).until(lambda d: d.find_element(By.ID, "products"))

(mX, mY) = three.getMouse()
three.actions.move_by_offset(mX, mY)
three.mimicHumanMouseMoveToElem(products)
three.actions.perform()
time.sleep(3)
three.actions.reset_actions()

# productsToView = []
# productLinks = WebDriverWait(three.driver, 3).until(lambda d: d.find_elements(By.CLASS_NAME, "linkWrapper--7sh4o"))

# for p in productLinks:
# 	span = WebDriverWait(three.driver, 3).until(lambda d: p.find_element(By.TAG_NAME, "span"))
# 	productsToView.append(span.text)

# for p in productsToView:
# 	(mX, mY) = three.getMouse()
# 	three.actions.move_by_offset(mX, mY)
# 	link = WebDriverWait(three.driver, 10).until(lambda d: d.find_element(By.XPATH, "//*[contains(text(),'" + p + "')]"))
# 	three.mimicHumanMouseMoveToElem(link)
# 	three.actions.click()
# 	three.actions.perform()
# 	three.actions.reset_actions()
# 	time.sleep(1)
# 	footer = WebDriverWait(three.driver, 3).until(lambda d: d.find_element(By.TAG_NAME, "footer"))
# 	three.mimicHumanScroll(footer)
# 	three.actions.reset_actions()	
# 	three.driver.back()
# 	three.injectMouse()
# 	(mX, mY) = three.getMouse()
# 	three.actions.move_by_offset(mX, mY)
# 	products = WebDriverWait(three.driver, 3).until(lambda d: d.find_element(By.ID, "products"))
# 	three.mimicHumanMouseMoveToElem(products)
# 	three.actions.perform()
# 	three.actions.reset_actions()
# 	time.sleep(1)

print("Go to resources")
(mX, mY) = three.getMouse()
three.actions.move_by_offset(mX, mY)

res = WebDriverWait(three.driver, 3).until(lambda d: d.find_element(By.ID, "resources"))
three.mimicHumanMouseMoveToElem(res)
three.actions.click()
three.actions.perform()
three.actions.reset_actions()
footer = WebDriverWait(three.driver, 3).until(lambda d: d.find_element(By.TAG_NAME, "footer"))
three.mimicHumanScroll(footer)
three.actions.reset_actions()
time.sleep(2)

print("Go to partners")
print(three.actions)
three.driver.back()
three.actions.reset_actions()
par = WebDriverWait(three.driver, 3).until(lambda d: d.find_element(By.ID, "partners"))

print("Set up mouse")
three.injectMouse()
(mX, mY) = three.getMouse()
print((mX, mY))
three.actions.reset_actions()
three.actions.move_by_offset(mX, mY)

print(three.actions)
print(par.location)
three.mimicHumanMouseMoveToElem(par)
three.actions.click()
three.actions.perform()
three.actions.reset_actions()
footer = WebDriverWait(three.driver, 3).until(lambda d: d.find_element(By.TAG_NAME, "footer"))
three.mimicHumanScroll(footer)
three.actions.reset_actions()
time.sleep(1)


time.sleep(3)
three.driver.quit()