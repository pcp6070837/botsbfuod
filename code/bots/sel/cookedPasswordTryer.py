import three
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

import time

import random, string

def randomword(length):
   letters = string.ascii_lowercase
   return ''.join(random.choice(letters) for i in range(length))

url = "https://jscrambler.pt"

three.driver.get(url)

print("Accepting Cookies")
cookies = WebDriverWait(three.driver, 3).until(lambda d: d.find_element(By.ID, "CybotCookiebotDialogBodyLevelButtonLevelOptinAllowAll"))
three.mimicHumanMouseMoveToElem(cookies)
three.actions.click()
three.actions.perform()
three.actions.reset_actions()
time.sleep(3)

(mX, mY) = three.getMouse()
three.actions.move_by_offset(mX, mY)
print("Clicking Login")
login = WebDriverWait(three.driver, 3).until(lambda d: d.find_element(By.LINK_TEXT, "Login"))
three.mimicHumanMouseMoveToElem(login)
three.actions.click()
three.actions.perform()
three.actions.reset_actions()
time.sleep(2)

uname = WebDriverWait(three.driver, 3).until(lambda d: d.find_element(By.ID, "username"))
passwd = WebDriverWait(three.driver, 3).until(lambda d: d.find_element(By.ID, "password"))

btn = WebDriverWait(three.driver, 3).until(lambda d: d.find_element(By.ID, "formSubmitButton"))

(mX, mY) = three.getMouse()
three.actions.move_by_offset(mX, mY)
print("Typing Uname")
unameStr = "fake@email.com"
three.mimicHumanMouseMoveToElem(uname)
three.actions.click()
three.mimicHumanType(three.actions, unameStr)
three.actions.perform()
three.actions.reset_actions()

for i in range(5):

	(mX, mY) = three.getMouse()
	three.actions.move_by_offset(mX, mY)

	passwdStr = randomword(i+10)
	three.mimicHumanMouseMoveToElem(passwd)
	three.actions.click()
	three.mimicHumanType(three.actions, passwdStr)
	three.actions.perform()
	three.actions.reset_actions()

	(mX, mY) = three.getMouse()
	three.actions.move_by_offset(mX, mY)
	three.mimicHumanMouseMoveToElem(btn)
	three.actions.click()
	three.actions.perform()
	three.actions.reset_actions()

	(mX, mY) = three.getMouse()
	three.actions.move_by_offset(mX, mY)
	three.mimicHumanMouseMoveToElem(passwd)
	three.actions.click()
	three.actions.perform()
	three.actions.reset_actions()
	
	while passwd.get_attribute("value") != "":
		passwd.send_keys(Keys.BACK_SPACE)
		j = random.randrange(1, 5)/100
		time.sleep(j)

	three.actions.reset_actions()

time.sleep(15)
three.driver.quit()