from selenium import webdriver

from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By


def document_initialised(driver):
    return driver.execute_script("return true") # argument here is JS script, can have array of args for script as 2nd arg 

driver = webdriver.Firefox()

driver.get("http://localhost/capazpsicologia")
#WebDriverWait(driver, timeout=10).until(document_initialised) # this will run on a loop every 10 ms to check is the condition in the func is met
el = WebDriverWait(driver, timeout=3).until(lambda d: d.find_element(By.TAG_NAME, "p"))

#print(el) #THIS WORKS prints to console
#driver.implicitly_wait(30)

#xOffset = 100
#yOffset = 100

#webdriver.ActionChains(driver).move_by_offset(xOffset,yOffset).perform()
