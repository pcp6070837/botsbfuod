import time

from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys

options = webdriver.FirefoxOptions()
options.set_preference("general.useragent.override", "fabdata")

driver = webdriver.Firefox(options=options)
driver.fullscreen_window()
driver.get("https://jscrambler.pt")

time.sleep(300)

driver.quit()