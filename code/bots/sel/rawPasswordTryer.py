import time

from selenium import webdriver

from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys

import random, string

def randomword(length):
   letters = string.ascii_lowercase
   return ''.join(random.choice(letters) for i in range(length))



url = "https://jscrambler.com/login"


options = webdriver.FirefoxOptions()
options.set_preference("general.useragent.override", "RavioliPestoBot")

driver = webdriver.Firefox(options=options)
driver.get(url)

cookies = WebDriverWait(driver, 3).until(lambda d: d.find_element(By.ID, "CybotCookiebotDialogBodyLevelButtonLevelOptinAllowAll"))
cookies.click()

uname = WebDriverWait(driver, 3).until(lambda d: d.find_element(By.ID, "username"))
passwd = WebDriverWait(driver, 3).until(lambda d: d.find_element(By.ID, "password"))

unameStr = "fake@email.com"
uname.send_keys(unameStr)

for i in range(10):
	passwdStr = randomword(10)
	passwd.clear()
	passwd.send_keys(passwdStr)
	WebDriverWait(driver, 3).until(lambda d: d.find_element(By.ID, "formSubmitButton")).click()
	time.sleep(2)



driver.quit()