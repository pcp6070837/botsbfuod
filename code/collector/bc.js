"use strict";

// Pressure.js library needed to read mouse and touch pressure
const Pressure = require("pressure");
const Stats = require("./stats");

let statistics = new Stats();

function statCleaner(key, value) {
    if (
        key == "keys" ||
        key == "flights" ||
        key == "flightTimestamp" ||
        key == "lastStrike" ||
        key == "currClickPresh" ||
        key == "currTouchPresh" ||
        key == "currPointerPresh" ||
        key == "noClickP" ||
        key == "noTouchesP" ||
        key == "noPointerP"
    )
        return undefined;
    if (key == "mouseM" || key == "scrollM" || key == "touchM") {
        let newVal = { ...value };
        delete newVal.x;
        delete newVal.y;
        delete newVal.timestamp;
        return newVal;
    }

    if (key == "touch") {
        let newVal = { ...value };
        delete newVal.touchTimestamp;
        return newVal;
    }

    if (key == "buttons") {
        let newVal = { ...value };
        delete newVal[0].clickTimestamp;
        delete newVal[1].clickTimestamp;
        delete newVal[2].clickTimestamp;
        delete newVal[3].clickTimestamp;
        delete newVal[4].clickTimestamp;
        return newVal;
    } else return value;
}

function sendStats() {
    statistics.page = document.location.href;
    let data = new FormData();
    data.append("obj", JSON.stringify(statistics, statCleaner));
    navigator.sendBeacon(
        "http://localhost/capazpsicologia/testLog.php",
        JSON.stringify(statistics, statCleaner)
    );
}

function checkForMobile(){

    ((a)  => {
        if ((
            /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(
                a
            ) ||
            /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(
                a.substr(0, 4)
            )
        ))  {
                statistics.mobile = true;
        }
    })(
        navigator.userAgent || navigator.vendor || window.opera,
    );

    if (window.matchMedia("only screen and (max-width: 1024px)").matches) statistics.mobile = true;

}

// To measure average click/touch/pointer pressure
Pressure.set(
    "*",
    {
        change: (force, _) => {
            statistics.currClickPresh = force;
            statistics.setTime();
        },
        end: function () {
            statistics.avgClickPresh =
                (statistics.avgClickPresh * statistics.noClickP +
                    statistics.currClickPresh) /
                (statistics.noClickP + 1);
            statistics.noClickP++;
            statistics.setTime();
        },
    },
    { only: "mouse", preventSelect: false, polyfil:false }
);

Pressure.set(
    "*",
    {
        change: (force, _) => {
            statistics.currTouchPresh = force;
            statistics.setTime();
        },
        end: function () {
            statistics.avgTouchPresh =
                (statistics.avgTouchPresh * statistics.noTouchesP +
                    statistics.currTouchPresh) /
                (statistics.noTouchesP + 1);
            statistics.noTouchesP++;
            statistics.setTime();
        },
    },
    { only: "touch", preventSelect: false, polyfil:false }
);

Pressure.set(
    "*",
    {
        change: (force, _) => {
            statistics.currPointerPresh = force;
            statistics.setTime();
        },
        end: function () {
            statistics.avgPointerPresh =
                (statistics.avgPointerPresh * statistics.noPointerP +
                    statistics.currPointerPresh) /
                (statistics.noPointerP + 1);
            statistics.noPointerP++;
            statistics.setTime();
        },
    },
    { only: "pointer", preventSelect: false, polyfil:false }
);

// Counting Focus Shifts
window.addEventListener("focus", () => {
    statistics.fs++;
    statistics.setTime();
});

// Keeping track of cuts, copys and pastes
window.addEventListener("cut", () => statistics.cuts++);
window.addEventListener("copy", () => statistics.cpys++);
window.addEventListener("paste", () => statistics.pastes++);

// Keydown Events
window.addEventListener("keydown", (event) => {
    statistics.strikes++;
    const re = /^[a-zA-Z0-9]$/;

    if (event.key === "Backspace" || event.key === "Delete")
        statistics.delKeys++;
    else if (re.test(event.key)) statistics.alphanumKeys++;
    else {
        statistics.otherKeys++;
        console.log("got key: " + event.key);
    }

    statistics.strikeStdDev = Stats.standardDeviation([
        statistics.alphanumKeys,
        statistics.delKeys,
        statistics.otherKeys,
    ]);

    let entA = isNaN(
        (statistics.alphanumKeys / statistics.strikes) *
            Math.log2(statistics.alphanumKeys / statistics.strikes)
    )
        ? 0
        : (statistics.alphanumKeys / statistics.strikes) *
          Math.log2(statistics.alphanumKeys / statistics.strikes);
    let entD = isNaN(
        (statistics.delKeys / statistics.strikes) *
            Math.log2(statistics.delKeys / statistics.strikes)
    )
        ? 0
        : (statistics.delKeys / statistics.strikes) *
          Math.log2(statistics.delKeys / statistics.strikes);
    let entO = isNaN(
        (statistics.otherKeys / statistics.strikes) *
            Math.log2(statistics.otherKeys / statistics.strikes)
    )
        ? 0
        : (statistics.otherKeys / statistics.strikes) *
          Math.log2(statistics.otherKeys / statistics.strikes);

    statistics.strikeEntropy = 0 - entA - entD - entO;

    if (statistics.strikeEntropy == null) statistics.strikeEntropy = 0;

    if (!statistics.keys.has(event.key)) {
        statistics.keys.set(event.key, Date.now());
    }
});

let kbSendData = setTimeout(sendStats, 500);
// Keyup Events
window.addEventListener("keyup", (event) => {
    statistics.setTime();
    clearTimeout(kbSendData);
    if (statistics.flightTimestamp === 0)
        statistics.flightTimestamp = Date.now();
    // To calculate avg time holding key
    if (statistics.keys.has(event.key)) {
        statistics.keyPresses++;
        let dt = Date.now() - statistics.keys.get(event.key);
        statistics.avgKeyHold =
            ((statistics.keyPresses - 1) * statistics.avgKeyHold + dt) /
            statistics.keyPresses;

        if (statistics.avgKeyHold == null) statistics.avgKeyHold = 0;
        statistics.keys.delete(event.key);

        dt = Date.now() - statistics.lastStrike;
        let lastSS = statistics.strikeSpeed;
        statistics.strikeSpeed = statistics.keyPresses / dt;
        statistics.strikeAcel = (lastSS - statistics.strikeSpeed) / dt;
        statistics.lastStrike = Date.now();
    }

    // To calculate key flight time
    let ft = Date.now() - statistics.flightTimestamp;
    statistics.flights++;
    statistics.avgFlightTime =
        (statistics.flights * statistics.avgFlightTime + ft) /
        (statistics.flights + 1);
    statistics.flightTimestamp = Date.now();

    kbSendData = setTimeout(sendStats, 500);
});

// Touch Screen movement event
window.addEventListener("touchmove", (event) => {
    statistics.touchM.move(
        event.touches[0].pageX,
        event.touches[0].pageY,
        false
    );
});

// Touch Start event
window.addEventListener("touchstart", () => {
    statistics.touch.touchTimestamp = Date.now();
    statistics.touch.touches++;
});

// Touch End event
window.addEventListener("touchend", () => {
    statistics.setTime();
    let dt = Date.now() - statistics.touch.touchTimestamp;
    statistics.touch.avgTouchHold =
        (statistics.touch.touches * statistics.touch.avgTouchHold + dt) /
        (statistics.touch.touches + 1);
    statistics.touch.touchTimestamp = Date.now();
    sendStats();
});

let mouseSendData = setTimeout(sendStats, 500);

// Mouse movement event
window.addEventListener("mousemove", (event) => {
    statistics.setTime();
    clearTimeout(mouseSendData);
    statistics.mouseM.move(event.pageX, event.pageY, false);
    mouseSendData = setTimeout(sendStats, 500);
});

// Mousedown Events
window.addEventListener("mousedown", (event) => {
    let i = event.button;
    statistics.buttons[i].clickTimestamp = Date.now();
    statistics.buttons[i].clicks++;
});

// Mouseup Events
window.addEventListener("mouseup", (event) => {
    let i = event.button;
    let ct = Date.now() - statistics.buttons[i].clickTimestamp;
    statistics.buttons[i].avgClickHold =
        (statistics.buttons[i].avgClickHold * statistics.buttons[i].clicks +
            ct) /
        (statistics.buttons[i].clicks + 1);
    statistics.buttons[i].clickTimestamp = Date.now();
});

// Page Load Events
window.addEventListener("load", () => {
    statistics.setTime();
    
    checkForMobile();

    if (window.performance.navigation) {
        if (window.performance.navigation === 1) {
            statistics.reload = true;
        }

        if (window.performance.navigation === 2) {
            statistics.bckFwd = true;
        }
    }

    if (window.performance.getEntriesByType("navigation")) {
        let navType = window.performance.getEntriesByType("navigation")[0].type;

        if (navType == "reload") {
            statistics.reload = true;
        }

        if (navType == "back_forward") {
            statistics.bckFwd = true;
        }
    }

    sendStats();
});

let scrollSendData = setTimeout(sendStats, 500);
//Scroll Events
window.addEventListener("scroll", () => {
    statistics.setTime();
    clearTimeout(scrollSendData);
    statistics.scrollM.move(window.scollX, window.scrollY, false);
    scrollSendData = setTimeout(sendStats, 500);
});

//Unload event, plain AJAX won't work here, but sendBeacon will
window.addEventListener("unload", () => {
    statistics.setTime();
    sendStats();
});
