//Describes a movement in terms of displacement, speed, aceleration and their standard deviations
class Move {
    constructor() {
        this.timestamp = 0;
        (this.x = 0), (this.y = 0);
        (this.totalX = 0), (this.totalY = 0);
        (this.speedX = 0), (this.speedY = 0);
        (this.acelX = 0), (this.acelY = 0);
        (this.jerkX = 0), (this.jerkY = 0);
    }

    move(x, y, debug) {
        let now = Date.now();
        let dt = now - this.timestamp;
        let distanceX, distanceY;
        distanceX = Math.abs(this.x - x);
        distanceY = Math.abs(this.y - y);
        this.totalX += distanceX;
        this.totalY += distanceY;
        this.x = x;
        this.y = y;
        this.timestamp = now;

        let lastSX = this.speedX;
        let lastSY = this.speedY;

        this.speedX = distanceX / dt;
        this.speedY = distanceY / dt;

        let lastAX = this.acelX;
        let lastAY = this.acelY;

        this.acelX = (this.speedX - lastSX) / dt;
        this.acelY = (this.speedY - lastSY) / dt;

        this.jerkX = (this.acelX - lastAX) / dt;
        this.jerkY = (this.acelY - lastAY) / dt;

        if (debug) console.log(this);
    }
}

module.exports = Move;
