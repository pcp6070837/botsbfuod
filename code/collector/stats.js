let Move = require("./move");

const start = Date.now();

class Stats {
    constructor() {
        // Since script loaded
        this.timeElapsed = 0;
        // Is script running on mobile device?
        this.mobile = false;
        //  Booleans to see if page was reloaded or loaded from cache through the arrows in the browser window
        (this.reload = false), (this.bckFwd = false);
        // Focus Shift counter: counter for how many times the window has lost and gained focus
        this.fs = 0;
        // Movement Statistics
        this.mouseM = new Move();
        this.touchM = new Move();
        this.scrollM = new Move();

        // Click Pressure Statistics not currently used
        this.currClickPresh = 0;
        this.avgClickPresh = 0;
        this.noClickP = 0;
        this.currTouchPresh = 0;
        this.avgTouchPresh = 0;
        this.noTouchesP = 0;
        this.currPointerPresh = 0;
        this.avgPointerPresh = 0;
        this.noPointerP = 0;

        // One for each button, 0 is Left, 1 is Middle/Wheel and 2 is Right
        this.buttons = [
            { clickTimestamp: 0, clicks: 0, avgClickHold: 0 },
            { clickTimestamp: 0, clicks: 0, avgClickHold: 0 },
            { clickTimestamp: 0, clicks: 0, avgClickHold: 0 },
            { clickTimestamp: 0, clicks: 0, avgClickHold: 0 },
            { clickTimestamp: 0, clicks: 0, avgClickHold: 0 },
        ];

        // Obj to keep track of touch data
        this.touch = { touchTimestamp: 0, touches: 0, avgTouchHold: 0 };

        this.delKeys = 0; // Number of times delete or backspace key was pressed
        this.alphanumKeys = 0; // Number of [a-zA-Z0-1] characters hit
        this.otherKeys = 0; // Other characters
        this.strikeStdDev = 0;
        this.strikeEntropy = 0;

        (this.cuts = 0), (this.cpys = 0), (this.pastes = 0);

        // Map with the key and the time holding it n vars to calculate avg key hold and typing speeds
        this.keys = new Map();
        (this.avgKeyHold = 0),
            (this.keyPresses = 0),
            (this.strikes = 0),
            (this.lastStrike = 0),
            (this.strikeSpeed = 0),
            (this.strikeAcel = 0);

        // Vars to help calculate key flight time
        (this.avgFlightTime = 0),
            (this.flights = 0),
            (this.flightTimestamp = 0);
    }

    static mean(arr) {
        return (
            arr.reduce((acc, curr) => {
                return acc + curr;
            }, 0) / arr.length
        );
    }

    static standardDeviation(arr) {
        let mean = this.mean(arr);

        let squareRay = arr.map((el) => {
            return (el - mean) ** 2;
        });

        let sum = squareRay.reduce((acc, curr) => acc + curr, 0);

        return Math.sqrt(sum / arr.length);
    }

    setTime() {
        this.timeElapsed = Date.now() - start;
    }
}

module.exports = Stats;
