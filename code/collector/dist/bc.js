(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
"use strict"; // Pressure.js library needed to read mouse and touch pressure

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var Pressure = require("pressure");

var Stats = require("./stats");

var statistics = new Stats();

function statCleaner(key, value) {
  if (key == "keys" || key == "flights" || key == "flightTimestamp" || key == "lastStrike" || key == "currClickPresh" || key == "currTouchPresh" || key == "currPointerPresh" || key == "noClickP" || key == "noTouchesP" || key == "noPointerP") return undefined;

  if (key == "mouseM" || key == "scrollM" || key == "touchM") {
    var newVal = _objectSpread({}, value);

    delete newVal.x;
    delete newVal.y;
    delete newVal.timestamp;
    return newVal;
  }

  if (key == "touch") {
    var _newVal = _objectSpread({}, value);

    delete _newVal.touchTimestamp;
    return _newVal;
  }

  if (key == "buttons") {
    var _newVal2 = _objectSpread({}, value);

    delete _newVal2[0].clickTimestamp;
    delete _newVal2[1].clickTimestamp;
    delete _newVal2[2].clickTimestamp;
    delete _newVal2[3].clickTimestamp;
    delete _newVal2[4].clickTimestamp;
    return _newVal2;
  } else return value;
}

function sendStats() {
  statistics.page = document.location.href;
  var data = new FormData();
  data.append("obj", JSON.stringify(statistics, statCleaner));
  navigator.sendBeacon("http://localhost/capazpsicologia/testLog.php", JSON.stringify(statistics, statCleaner));
}

function checkForMobile() {
  (function (a) {
    if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) {
      statistics.mobile = true;
    }
  })(navigator.userAgent || navigator.vendor || window.opera);

  if (window.matchMedia("only screen and (max-width: 1024px)").matches) statistics.mobile = true;
} // To measure average click/touch/pointer pressure


Pressure.set("*", {
  change: function change(force, _) {
    statistics.currClickPresh = force;
    statistics.setTime();
  },
  end: function end() {
    statistics.avgClickPresh = (statistics.avgClickPresh * statistics.noClickP + statistics.currClickPresh) / (statistics.noClickP + 1);
    statistics.noClickP++;
    statistics.setTime();
  }
}, {
  only: "mouse",
  preventSelect: false,
  polyfil: false
});
Pressure.set("*", {
  change: function change(force, _) {
    statistics.currTouchPresh = force;
    statistics.setTime();
  },
  end: function end() {
    statistics.avgTouchPresh = (statistics.avgTouchPresh * statistics.noTouchesP + statistics.currTouchPresh) / (statistics.noTouchesP + 1);
    statistics.noTouchesP++;
    statistics.setTime();
  }
}, {
  only: "touch",
  preventSelect: false,
  polyfil: false
});
Pressure.set("*", {
  change: function change(force, _) {
    statistics.currPointerPresh = force;
    statistics.setTime();
  },
  end: function end() {
    statistics.avgPointerPresh = (statistics.avgPointerPresh * statistics.noPointerP + statistics.currPointerPresh) / (statistics.noPointerP + 1);
    statistics.noPointerP++;
    statistics.setTime();
  }
}, {
  only: "pointer",
  preventSelect: false,
  polyfil: false
}); // Counting Focus Shifts

window.addEventListener("focus", function () {
  statistics.fs++;
  statistics.setTime();
}); // Keeping track of cuts, copys and pastes

window.addEventListener("cut", function () {
  return statistics.cuts++;
});
window.addEventListener("copy", function () {
  return statistics.cpys++;
});
window.addEventListener("paste", function () {
  return statistics.pastes++;
}); // Keydown Events

window.addEventListener("keydown", function (event) {
  statistics.strikes++;
  var re = /^[a-zA-Z0-9]$/;
  if (event.key === "Backspace" || event.key === "Delete") statistics.delKeys++;else if (re.test(event.key)) statistics.alphanumKeys++;else {
    statistics.otherKeys++;
    console.log("got key: " + event.key);
  }
  statistics.strikeStdDev = Stats.standardDeviation([statistics.alphanumKeys, statistics.delKeys, statistics.otherKeys]);
  var entA = isNaN(statistics.alphanumKeys / statistics.strikes * Math.log2(statistics.alphanumKeys / statistics.strikes)) ? 0 : statistics.alphanumKeys / statistics.strikes * Math.log2(statistics.alphanumKeys / statistics.strikes);
  var entD = isNaN(statistics.delKeys / statistics.strikes * Math.log2(statistics.delKeys / statistics.strikes)) ? 0 : statistics.delKeys / statistics.strikes * Math.log2(statistics.delKeys / statistics.strikes);
  var entO = isNaN(statistics.otherKeys / statistics.strikes * Math.log2(statistics.otherKeys / statistics.strikes)) ? 0 : statistics.otherKeys / statistics.strikes * Math.log2(statistics.otherKeys / statistics.strikes);
  statistics.strikeEntropy = 0 - entA - entD - entO;
  if (statistics.strikeEntropy == null) statistics.strikeEntropy = 0;

  if (!statistics.keys.has(event.key)) {
    statistics.keys.set(event.key, Date.now());
  }
});
var kbSendData = setTimeout(sendStats, 500); // Keyup Events

window.addEventListener("keyup", function (event) {
  statistics.setTime();
  clearTimeout(kbSendData);
  if (statistics.flightTimestamp === 0) statistics.flightTimestamp = Date.now(); // To calculate avg time holding key

  if (statistics.keys.has(event.key)) {
    statistics.keyPresses++;
    var dt = Date.now() - statistics.keys.get(event.key);
    statistics.avgKeyHold = ((statistics.keyPresses - 1) * statistics.avgKeyHold + dt) / statistics.keyPresses;
    if (statistics.avgKeyHold == null) statistics.avgKeyHold = 0;
    statistics.keys["delete"](event.key);
    dt = Date.now() - statistics.lastStrike;
    var lastSS = statistics.strikeSpeed;
    statistics.strikeSpeed = statistics.keyPresses / dt;
    statistics.strikeAcel = (lastSS - statistics.strikeSpeed) / dt;
    statistics.lastStrike = Date.now();
  } // To calculate key flight time


  var ft = Date.now() - statistics.flightTimestamp;
  statistics.flights++;
  statistics.avgFlightTime = (statistics.flights * statistics.avgFlightTime + ft) / (statistics.flights + 1);
  statistics.flightTimestamp = Date.now();
  kbSendData = setTimeout(sendStats, 500);
}); // Touch Screen movement event

window.addEventListener("touchmove", function (event) {
  statistics.touchM.move(event.touches[0].pageX, event.touches[0].pageY, false);
}); // Touch Start event

window.addEventListener("touchstart", function () {
  statistics.touch.touchTimestamp = Date.now();
  statistics.touch.touches++;
}); // Touch End event

window.addEventListener("touchend", function () {
  statistics.setTime();
  var dt = Date.now() - statistics.touch.touchTimestamp;
  statistics.touch.avgTouchHold = (statistics.touch.touches * statistics.touch.avgTouchHold + dt) / (statistics.touch.touches + 1);
  statistics.touch.touchTimestamp = Date.now();
  sendStats();
});
var mouseSendData = setTimeout(sendStats, 500); // Mouse movement event

window.addEventListener("mousemove", function (event) {
  statistics.setTime();
  clearTimeout(mouseSendData);
  statistics.mouseM.move(event.pageX, event.pageY, false);
  mouseSendData = setTimeout(sendStats, 500);
}); // Mousedown Events

window.addEventListener("mousedown", function (event) {
  var i = event.button;
  statistics.buttons[i].clickTimestamp = Date.now();
  statistics.buttons[i].clicks++;
}); // Mouseup Events

window.addEventListener("mouseup", function (event) {
  var i = event.button;
  var ct = Date.now() - statistics.buttons[i].clickTimestamp;
  statistics.buttons[i].avgClickHold = (statistics.buttons[i].avgClickHold * statistics.buttons[i].clicks + ct) / (statistics.buttons[i].clicks + 1);
  statistics.buttons[i].clickTimestamp = Date.now();
}); // Page Load Events

window.addEventListener("load", function () {
  statistics.setTime();
  checkForMobile();

  if (window.performance.navigation) {
    if (window.performance.navigation === 1) {
      statistics.reload = true;
    }

    if (window.performance.navigation === 2) {
      statistics.bckFwd = true;
    }
  }

  if (window.performance.getEntriesByType("navigation")) {
    var navType = window.performance.getEntriesByType("navigation")[0].type;

    if (navType == "reload") {
      statistics.reload = true;
    }

    if (navType == "back_forward") {
      statistics.bckFwd = true;
    }
  }

  sendStats();
});
var scrollSendData = setTimeout(sendStats, 500); //Scroll Events

window.addEventListener("scroll", function () {
  statistics.setTime();
  clearTimeout(scrollSendData);
  statistics.scrollM.move(window.scollX, window.scrollY, false);
  scrollSendData = setTimeout(sendStats, 500);
}); //Unload event, plain AJAX won't work here, but sendBeacon will

window.addEventListener("unload", function () {
  statistics.setTime();
  sendStats();
});

},{"./stats":4,"pressure":3}],2:[function(require,module,exports){
"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

//Describes a movement in terms of displacement, speed, aceleration and their standard deviations
var Move = /*#__PURE__*/function () {
  function Move() {
    _classCallCheck(this, Move);

    this.timestamp = 0;
    this.x = 0, this.y = 0;
    this.totalX = 0, this.totalY = 0;
    this.speedX = 0, this.speedY = 0;
    this.acelX = 0, this.acelY = 0;
    this.jerkX = 0, this.jerkY = 0;
  }

  _createClass(Move, [{
    key: "move",
    value: function move(x, y, debug) {
      var now = Date.now();
      var dt = now - this.timestamp;
      var distanceX, distanceY;
      distanceX = Math.abs(this.x - x);
      distanceY = Math.abs(this.y - y);
      this.totalX += distanceX;
      this.totalY += distanceY;
      this.x = x;
      this.y = y;
      this.timestamp = now;
      var lastSX = this.speedX;
      var lastSY = this.speedY;
      this.speedX = distanceX / dt;
      this.speedY = distanceY / dt;
      var lastAX = this.acelX;
      var lastAY = this.acelY;
      this.acelX = (this.speedX - lastSX) / dt;
      this.acelY = (this.speedY - lastSY) / dt;
      this.jerkX = (this.acelX - lastAX) / dt;
      this.jerkY = (this.acelY - lastAY) / dt;
      if (debug) console.log(this);
    }
  }]);

  return Move;
}();

module.exports = Move;

},{}],3:[function(require,module,exports){
// Pressure v2.2.0 | Created By Stuart Yamartino | MIT License | 2015 - 2020
!function(e,t){"function"==typeof define&&define.amd?define([],t):"object"==typeof exports?module.exports=t():e.Pressure=t()}(this,function(){"use strict";function i(e){return(i="function"==typeof Symbol&&"symbol"==typeof Symbol.iterator?function(e){return typeof e}:function(e){return e&&"function"==typeof Symbol&&e.constructor===Symbol&&e!==Symbol.prototype?"symbol":typeof e})(e)}function e(e,t){if("function"!=typeof t&&null!==t)throw new TypeError("Super expression must either be null or a function");e.prototype=Object.create(t&&t.prototype,{constructor:{value:e,writable:!0,configurable:!0}}),t&&s(e,t)}function s(e,t){return(s=Object.setPrototypeOf||function(e,t){return e.__proto__=t,e})(e,t)}function t(s){var n=function(){if("undefined"==typeof Reflect||!Reflect.construct)return!1;if(Reflect.construct.sham)return!1;if("function"==typeof Proxy)return!0;try{return Date.prototype.toString.call(Reflect.construct(Date,[],function(){})),!0}catch(e){return!1}}();return function(){var e,t=r(s);return e=n?(e=r(this).constructor,Reflect.construct(t,arguments,e)):t.apply(this,arguments),t=this,!(e=e)||"object"!==i(e)&&"function"!=typeof e?function(e){if(void 0!==e)return e;throw new ReferenceError("this hasn't been initialised - super() hasn't been called")}(t):e}}function r(e){return(r=Object.setPrototypeOf?Object.getPrototypeOf:function(e){return e.__proto__||Object.getPrototypeOf(e)})(e)}function o(e,t){if(!(e instanceof t))throw new TypeError("Cannot call a class as a function")}function n(e,t){for(var s=0;s<t.length;s++){var n=t[s];n.enumerable=n.enumerable||!1,n.configurable=!0,"value"in n&&(n.writable=!0),Object.defineProperty(e,n.key,n)}}function u(e,t,s){return t&&n(e.prototype,t),s&&n(e,s),e}var h={set:function(e,t,s){y(e,t,s)},config:function(e){p.set(e)},map:function(){return v.apply(null,arguments)}},c=function(){function n(e,t,s){o(this,n),this.routeEvents(e,t,s),this.preventSelect(e,s)}return u(n,[{key:"routeEvents",value:function(e,t,s){var n=p.get("only",s);this.adapter=!b||"mouse"!==n&&null!==n?!m||"pointer"!==n&&null!==n?!g||"touch"!==n&&null!==n?new l(e,t).bindUnsupportedEvent():new d(e,t,s).bindEvents():new f(e,t,s).bindEvents():new a(e,t,s).bindEvents()}},{key:"preventSelect",value:function(e,t){p.get("preventSelect",t)&&(e.style.webkitTouchCallout="none",e.style.webkitUserSelect="none",e.style.khtmlUserSelect="none",e.style.MozUserSelect="none",e.style.msUserSelect="none",e.style.userSelect="none")}}]),n}(),l=function(){function n(e,t,s){o(this,n),this.el=e,this.block=t,this.options=s,this.pressed=!1,this.deepPressed=!1,this.nativeSupport=!1,this.runningPolyfill=!1,this.runKey=Math.random()}return u(n,[{key:"setPressed",value:function(e){this.pressed=e}},{key:"setDeepPressed",value:function(e){this.deepPressed=e}},{key:"isPressed",value:function(){return this.pressed}},{key:"isDeepPressed",value:function(){return this.deepPressed}},{key:"add",value:function(e,t){this.el.addEventListener(e,t,!1)}},{key:"runClosure",value:function(e){e in this.block&&this.block[e].apply(this.el,Array.prototype.slice.call(arguments,1))}},{key:"fail",value:function(e,t){p.get("polyfill",this.options)?this.runKey===t&&this.runPolyfill(e):this.runClosure("unsupported",e)}},{key:"bindUnsupportedEvent",value:function(){var t=this;this.add(g?"touchstart":"mousedown",function(e){return t.runClosure("unsupported",e)})}},{key:"_startPress",value:function(e){!1===this.isPressed()&&(this.runningPolyfill=!1,this.setPressed(!0),this.runClosure("start",e))}},{key:"_startDeepPress",value:function(e){this.isPressed()&&!1===this.isDeepPressed()&&(this.setDeepPressed(!0),this.runClosure("startDeepPress",e))}},{key:"_changePress",value:function(e,t){this.nativeSupport=!0,this.runClosure("change",e,t)}},{key:"_endDeepPress",value:function(){this.isPressed()&&this.isDeepPressed()&&(this.setDeepPressed(!1),this.runClosure("endDeepPress"))}},{key:"_endPress",value:function(){!1===this.runningPolyfill?(this.isPressed()&&(this._endDeepPress(),this.setPressed(!1),this.runClosure("end")),this.runKey=Math.random(),this.nativeSupport=!1):this.setPressed(!1)}},{key:"deepPress",value:function(e,t){.5<=e?this._startDeepPress(t):this._endDeepPress()}},{key:"runPolyfill",value:function(e){this.increment=0===p.get("polyfillSpeedUp",this.options)?1:10/p.get("polyfillSpeedUp",this.options),this.decrement=0===p.get("polyfillSpeedDown",this.options)?1:10/p.get("polyfillSpeedDown",this.options),this.setPressed(!0),this.runClosure("start",e),!1===this.runningPolyfill&&this.loopPolyfillForce(0,e)}},{key:"loopPolyfillForce",value:function(e,t){!1===this.nativeSupport&&(this.isPressed()?(this.runningPolyfill=!0,e=1<e+this.increment?1:e+this.increment,this.runClosure("change",e,t),this.deepPress(e,t),setTimeout(this.loopPolyfillForce.bind(this,e,t),10)):((e=e-this.decrement<0?0:e-this.decrement)<.5&&this.isDeepPressed()&&(this.setDeepPressed(!1),this.runClosure("endDeepPress")),0===e?(this.runningPolyfill=!1,this.setPressed(!0),this._endPress()):(this.runClosure("change",e,t),this.deepPress(e,t),setTimeout(this.loopPolyfillForce.bind(this,e,t),10))))}}]),n}(),a=function(){e(i,l);var n=t(i);function i(e,t,s){return o(this,i),n.call(this,e,t,s)}return u(i,[{key:"bindEvents",value:function(){this.add("webkitmouseforcewillbegin",this._startPress.bind(this)),this.add("mousedown",this.support.bind(this)),this.add("webkitmouseforcechanged",this.change.bind(this)),this.add("webkitmouseforcedown",this._startDeepPress.bind(this)),this.add("webkitmouseforceup",this._endDeepPress.bind(this)),this.add("mouseleave",this._endPress.bind(this)),this.add("mouseup",this._endPress.bind(this))}},{key:"support",value:function(e){!1===this.isPressed()&&this.fail(e,this.runKey)}},{key:"change",value:function(e){this.isPressed()&&0<e.webkitForce&&this._changePress(this.normalizeForce(e.webkitForce),e)}},{key:"normalizeForce",value:function(e){return this.reachOne(v(e,1,3,0,1))}},{key:"reachOne",value:function(e){return.995<e?1:e}}]),i}(),d=function(){e(i,l);var n=t(i);function i(e,t,s){return o(this,i),n.call(this,e,t,s)}return u(i,[{key:"bindEvents",value:function(){k?(this.add("touchforcechange",this.start.bind(this)),this.add("touchstart",this.support.bind(this,0))):this.add("touchstart",this.startLegacy.bind(this)),this.add("touchend",this._endPress.bind(this))}},{key:"start",value:function(e){0<e.touches.length&&(this._startPress(e),this.touch=this.selectTouch(e),this.touch&&this._changePress(this.touch.force,e))}},{key:"support",value:function(e,t,s){s=2<arguments.length&&void 0!==s?s:this.runKey;!1===this.isPressed()&&(e<=6?(e++,setTimeout(this.support.bind(this,e,t,s),10)):this.fail(t,s))}},{key:"startLegacy",value:function(e){this.initialForce=e.touches[0].force,this.supportLegacy(0,e,this.runKey,this.initialForce)}},{key:"supportLegacy",value:function(e,t,s,n){n!==this.initialForce?(this._startPress(t),this.loopForce(t)):e<=6?(e++,setTimeout(this.supportLegacy.bind(this,e,t,s,n),10)):this.fail(t,s)}},{key:"loopForce",value:function(e){this.isPressed()&&(this.touch=this.selectTouch(e),setTimeout(this.loopForce.bind(this,e),10),this._changePress(this.touch.force,e))}},{key:"selectTouch",value:function(e){if(1===e.touches.length)return this.returnTouch(e.touches[0],e);for(var t=0;t<e.touches.length;t++)if(e.touches[t].target===this.el||this.el.contains(e.touches[t].target))return this.returnTouch(e.touches[t],e)}},{key:"returnTouch",value:function(e,t){return this.deepPress(e.force,t),e}}]),i}(),f=function(){e(i,l);var n=t(i);function i(e,t,s){return o(this,i),n.call(this,e,t,s)}return u(i,[{key:"bindEvents",value:function(){this.add("pointerdown",this.support.bind(this)),this.add("pointermove",this.change.bind(this)),this.add("pointerup",this._endPress.bind(this)),this.add("pointerleave",this._endPress.bind(this))}},{key:"support",value:function(e){!1===this.isPressed()&&(0===e.pressure||.5===e.pressure||1<e.pressure?this.fail(e,this.runKey):(this._startPress(e),this._changePress(e.pressure,e)))}},{key:"change",value:function(e){this.isPressed()&&0<e.pressure&&.5!==e.pressure&&(this._changePress(e.pressure,e),this.deepPress(e.pressure,e))}}]),i}(),p={polyfill:!0,polyfillSpeedUp:1e3,polyfillSpeedDown:0,preventSelect:!0,only:null,get:function(e,t){return(t.hasOwnProperty(e)?t:this)[e]},set:function(e){for(var t in e)e.hasOwnProperty(t)&&this.hasOwnProperty(t)&&"get"!=t&&"set"!=t&&(this[t]=e[t])}},y=function(e,t,s){var n=2<arguments.length&&void 0!==s?s:{};if("string"==typeof e||e instanceof String)for(var i=document.querySelectorAll(e),r=0;r<i.length;r++)new c(i[r],t,n);else if(P(e))new c(e,t,n);else for(r=0;r<e.length;r++)new c(e[r],t,n)},P=function(e){return"object"===("undefined"==typeof HTMLElement?"undefined":i(HTMLElement))?e instanceof HTMLElement:e&&"object"===i(e)&&null!==e&&1===e.nodeType&&"string"==typeof e.nodeName},v=function(e,t,s,n,i){return(e-t)*(i-n)/(s-t)+n},b=!1,g=!1,m=!1,w=!1,k=!1;if("undefined"!=typeof window){if("undefined"!=typeof Touch)try{(Touch.prototype.hasOwnProperty("force")||"force"in new Touch)&&(w=!0)}catch(e){}g="ontouchstart"in window.document&&w,b="onmousemove"in window.document&&"onwebkitmouseforcechanged"in window.document&&!g,m="onpointermove"in window.document,k="ontouchforcechange"in window.document}return h});
},{}],4:[function(require,module,exports){
"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

var Move = require("./move");

var start = Date.now();

var Stats = /*#__PURE__*/function () {
  function Stats() {
    _classCallCheck(this, Stats);

    // Since script loaded
    this.timeElapsed = 0; // Is script running on mobile device?

    this.mobile = false; //  Booleans to see if page was reloaded or loaded from cache through the arrows in the browser window

    this.reload = false, this.bckFwd = false; // Focus Shift counter: counter for how many times the window has lost and gained focus

    this.fs = 0; // Movement Statistics

    this.mouseM = new Move();
    this.touchM = new Move();
    this.scrollM = new Move(); // Click Pressure Statistics not currently used

    this.currClickPresh = 0;
    this.avgClickPresh = 0;
    this.noClickP = 0;
    this.currTouchPresh = 0;
    this.avgTouchPresh = 0;
    this.noTouchesP = 0;
    this.currPointerPresh = 0;
    this.avgPointerPresh = 0;
    this.noPointerP = 0; // One for each button, 0 is Left, 1 is Middle/Wheel and 2 is Right

    this.buttons = [{
      clickTimestamp: 0,
      clicks: 0,
      avgClickHold: 0
    }, {
      clickTimestamp: 0,
      clicks: 0,
      avgClickHold: 0
    }, {
      clickTimestamp: 0,
      clicks: 0,
      avgClickHold: 0
    }, {
      clickTimestamp: 0,
      clicks: 0,
      avgClickHold: 0
    }, {
      clickTimestamp: 0,
      clicks: 0,
      avgClickHold: 0
    }]; // Obj to keep track of touch data

    this.touch = {
      touchTimestamp: 0,
      touches: 0,
      avgTouchHold: 0
    };
    this.delKeys = 0; // Number of times delete or backspace key was pressed

    this.alphanumKeys = 0; // Number of [a-zA-Z0-1] characters hit

    this.otherKeys = 0; // Other characters

    this.strikeStdDev = 0;
    this.strikeEntropy = 0;
    this.cuts = 0, this.cpys = 0, this.pastes = 0; // Map with the key and the time holding it n vars to calculate avg key hold and typing speeds

    this.keys = new Map();
    this.avgKeyHold = 0, this.keyPresses = 0, this.strikes = 0, this.lastStrike = 0, this.strikeSpeed = 0, this.strikeAcel = 0; // Vars to help calculate key flight time

    this.avgFlightTime = 0, this.flights = 0, this.flightTimestamp = 0;
  }

  _createClass(Stats, [{
    key: "setTime",
    value: function setTime() {
      this.timeElapsed = Date.now() - start;
    }
  }], [{
    key: "mean",
    value: function mean(arr) {
      return arr.reduce(function (acc, curr) {
        return acc + curr;
      }, 0) / arr.length;
    }
  }, {
    key: "standardDeviation",
    value: function standardDeviation(arr) {
      var mean = this.mean(arr);
      var squareRay = arr.map(function (el) {
        return Math.pow(el - mean, 2);
      });
      var sum = squareRay.reduce(function (acc, curr) {
        return acc + curr;
      }, 0);
      return Math.sqrt(sum / arr.length);
    }
  }]);

  return Stats;
}();

module.exports = Stats;

},{"./move":2}]},{},[1]);
