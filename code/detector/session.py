import time

import numpy as np

from dateutil import parser

# Maybe Session should keep track of no. of events

class Session:

	def __init__(self, point):

		if "sid" in point:
			self.key = point["sid"]
		else:
			self.key = point["ip"] + point["ua"]
		
		if point["page"] == "":
			point["page"] = "https://jscrambler.com"
		
		self.lp = point["page"]
		self.pc = 1

		# Total Time Script has been running
		self.totalTime = point["timeElapsed"]
		# Total no. of focus shifts
		self.totalFS = point["fs"]
		# Total no. of Alphanumeric Characters 
		self.totalAlphas = point["alphanumKeys"]
		# Total no. of Deletes
		self.totalDels = point["delKeys"]
		# Other keys
		self.totalOthers = point["otherKeys"]

		self.totalKeyDowns = point["strikes"]

		rat = self.totalAlphas + self.totalDels + self.totalOthers
		if (rat  < 1):
			rat = 1

		self.alphaRat = self.totalAlphas/rat
		self.delRat = self.totalDels/rat
		self.otherRat = self.totalOthers/rat

		self.totalKeyUps = point["keyPresses"]
	
		self.strikeStdDev = point["strikeStdDev"]
		self.strikeEntropy = point["strikeEntropy"]
		self.strikeSpeed = point["strikeSpeed"]
		self.strikeAcel = point["strikeAcel"]
		self.avgFlightTime = point["avgFlightTime"]
		self.avgKeyHold = point["avgKeyHold"]

		self.totalCuts = point["cuts"]
		self.totalCpys = point["cpys"]
		self.totalPastes = point["pastes"]
		
		self.totalTouches = point["touch"]["touches"]
		self.totalClick0 = point["buttons"]["0"]["clicks"]
		self.totalClick1 = point["buttons"]["1"]["clicks"]
		self.totalClick2 = point["buttons"]["2"]["clicks"]

		try:
			self.totalClick3 = point["buttons"]["3"]["clicks"]
			self.totalClick4 = point["buttons"]["4"]["clicks"]
		except KeyError:
			self.totalClick3 = 0
			self.totalClick4 = 0
		
		
		self.lastDate = point["date"]

		self.lastMouseX = point["mouseM"]["totalX"]
		self.lastMouseY = point["mouseM"]["totalY"]
		self.lastMouseSX = point["mouseM"]["speedX"]
		self.lastMouseSY = point["mouseM"]["speedY"]
		self.lastMouseAX = point["mouseM"]["acelX"]
		self.lastMouseAY = point["mouseM"]["acelY"]
		self.lastMouseJX = point["mouseM"]["jerkX"]
		self.lastMouseJY = point["mouseM"]["jerkY"]
		self.lastC0Hold =  point["buttons"]["0"]["avgClickHold"]
		self.lastC1Hold =  point["buttons"]["1"]["avgClickHold"]
		self.lastC2Hold =  point["buttons"]["2"]["avgClickHold"]
		try:
			self.lastC3Hold =  point["buttons"]["3"]["avgClickHold"]
			self.lastC4Hold =  point["buttons"]["4"]["avgClickHold"]
		except KeyError:
			self.lastC3Hold =  0
			self.lastC4Hold =  0

		self.lastTouchX = point["touchM"]["totalX"]
		self.lastTouchY = point["touchM"]["totalY"]
		self.lastTouchSX = point["touchM"]["speedX"]
		self.lastTouchSY = point["touchM"]["speedY"]
		self.lastTouchAX = point["touchM"]["acelX"]
		self.lastTouchAY = point["touchM"]["acelY"]
		self.lastTouchJX = point["touchM"]["jerkX"]
		self.lastTouchJY = point["touchM"]["jerkY"]
		self.lastTouchHold = point["touch"]["avgTouchHold"]

		self.lastScrollX = point["scrollM"]["totalX"]
		self.lastScrollY = point["scrollM"]["totalY"]
		self.lastScrollSX = point["scrollM"]["speedX"]
		self.lastScrollSY = point["scrollM"]["speedY"]
		self.lastScrollAX = point["scrollM"]["acelX"]
		self.lastScrollAY = point["scrollM"]["acelY"]
		self.lastScrollJX = point["scrollM"]["jerkX"]
		self.lastScrollJY = point["scrollM"]["jerkY"]


		self.lastClickPresh = point["avgClickPresh"]
		self.lastTouchPresh = point["avgTouchPresh"]
		self.lastPointerPresh = point["avgPointerPresh"]

		self.mobile = point["mobile"]
		self.reload = point["reload"]
		self.bckFwd = point["bckFwd"]




		self.lastPackts = {point["page"]:point}

	def isInTimeOut(self, point):
		dt1 = parser.parse(self.lastDate)
		dt2 = parser.parse(point["date"])

		dt1_ts = time.mktime(dt1.timetuple())
		dt2_ts = time.mktime(dt2.timetuple())

		return (dt2_ts > dt1_ts and (int(dt2_ts - dt1_ts)/60) <= 30)
	
	def isSameSession(self, point):
		if "sid" in point:
			key = point["sid"]
		else:
			key = point["ip"] + point["ua"]
		
		return (key == self.key and self.isInTimeOut(point))

	def addPoint(self, point):
		#Need to update vars... not gonna mess with advanced stats rn -> entropy ans movement variables
		lastTime = 0
		lastFs = 0
		lastAlphas = 0
		lastDels = 0
		lastOthers = 0
		lastCuts = 0
		lastCpys = 0
		lastPastes = 0
		lastKUs = 0
		lastKDs = 0
		lastMobile = False
		lastReload = False
		lastBckFwd = False
		lastTouches = 0
		lastC0 = 0
		lastC1 = 0
		lastC2 = 0
		lastC3 = 0
		lastC4 = 0

		self.pc += 1
		if point["page"] == "":
			point["page"] = self.lp

		if point["page"] in self.lastPackts:
			self.lp = point["page"]
			lastTime = self.lastPackts[point["page"]]["timeElapsed"]
			lastFs = self.lastPackts[point["page"]]["fs"]
			lastAlphas = self.lastPackts[point["page"]]["alphanumKeys"]
			lastDels = self.lastPackts[point["page"]]["delKeys"]
			lastOthers = self.lastPackts[point["page"]]["otherKeys"]
			lastCuts = self.lastPackts[point["page"]]["cuts"]
			lastCpys = self.lastPackts[point["page"]]["cpys"]
			lastPastes = self.lastPackts[point["page"]]["pastes"]
			lastKUs = self.lastPackts[point["page"]]["keyPresses"]
			lastKDs = self.lastPackts[point["page"]]["strikes"]
			lastMobile = self.lastPackts[point["page"]]["mobile"]
			lastReload = self.lastPackts[point["page"]]["reload"]
			lastBckFwd = self.lastPackts[point["page"]]["bckFwd"]
			lastTouches = self.lastPackts[point["page"]]["touch"]["touches"]
			lastC0 = self.lastPackts[point["page"]]["buttons"]["0"]["clicks"]
			lastC1 = self.lastPackts[point["page"]]["buttons"]["1"]["clicks"]
			lastC2 = self.lastPackts[point["page"]]["buttons"]["2"]["clicks"]
			try:
				lastC3 = point["buttons"]["3"]["clicks"]
				lastC4 = point["buttons"]["4"]["clicks"]
			except KeyError:
				lastC3 = 0
				lastC4 = 0
		
		# Accummulated vars

		# Total Time Script has been running
		self.totalTime += point["timeElapsed"] - lastTime
		# Total no. of focus shifts
		self.totalFS += point["fs"] - lastFs
		# Total no. of Alphanumeric Characters 
		self.totalAlphas += point["alphanumKeys"] - lastAlphas
		# Total no. of Deletes
		self.totalDels += point["delKeys"] - lastDels
		# Other keys
		self.totalOthers += point["otherKeys"] - lastOthers

		rat = self.totalAlphas + self.totalDels + self.totalOthers
		if (rat  < 1):
			rat = 1

		self.alphaRat = self.totalAlphas/rat
		self.delRat = self.totalDels/rat
		self.otherRat = self.totalOthers/rat


		self.totalCuts += point["cuts"] - lastCuts
		self.totalCpys += point["cpys"] - lastCpys
		self.totalPastes += point["pastes"] - lastPastes
		self.totalKeyDowns += point["strikes"] - lastKDs
		self.totalKeyUps += point["keyPresses"] - lastKUs

		self.totalTouches += point["touch"]["touches"] - lastTouches

		self.totalClick0 += point["buttons"]["0"]["clicks"] - lastC0
		self.totalClick1 += point["buttons"]["1"]["clicks"] - lastC1
		self.totalClick2 += point["buttons"]["2"]["clicks"] - lastC2
		try:
			self.totalClick3 += point["buttons"]["3"]["clicks"] - lastC3
			self.totalClick4 += point["buttons"]["4"]["clicks"] - lastC4
		except KeyError:
			self.totalClick3 += 0
			self.totalClick4 += 0


		# Need to mess w/ these stats still maybe...
		self.strikeStdDev = point["strikeStdDev"]
		self.strikeEntropy = point["strikeEntropy"]
		self.avgFlightTime = point["avgFlightTime"]
		self.avgKeyHold = point["avgKeyHold"]

		# Instant Vars
		self.lastDate = point["date"]
		# self.lastMouseMove = point["mouseM"]
		# self.lastTouchMove = point["touchM"]
		# self.lastScrollMove = point["scrollM"]

		self.lastMouseX = point["mouseM"]["totalX"]
		self.lastMouseY = point["mouseM"]["totalY"]
		self.lastMouseSX = point["mouseM"]["speedX"]
		self.lastMouseSY = point["mouseM"]["speedY"]
		self.lastMouseAX = point["mouseM"]["acelX"]
		self.lastMouseAY = point["mouseM"]["acelY"]
		self.lastMouseJX = point["mouseM"]["jerkX"]
		self.lastMouseJY = point["mouseM"]["jerkY"]

		self.lastTouchX = point["touchM"]["totalX"]
		self.lastTouchY = point["touchM"]["totalY"]
		self.lastTouchSX = point["touchM"]["speedX"]
		self.lastTouchSY = point["touchM"]["speedY"]
		self.lastTouchAX = point["touchM"]["acelX"]
		self.lastTouchAY = point["touchM"]["acelY"]
		self.lastTouchJX = point["touchM"]["jerkX"]
		self.lastTouchJY = point["touchM"]["jerkY"]
		self.lastTouchHold = point["touch"]["avgTouchHold"]

		self.lastScrollX = point["scrollM"]["totalX"]
		self.lastScrollY = point["scrollM"]["totalY"]
		self.lastScrollSX = point["scrollM"]["speedX"]
		self.lastScrollSY = point["scrollM"]["speedY"]
		self.lastScrollAX = point["scrollM"]["acelX"]
		self.lastScrollAY = point["scrollM"]["acelY"]
		self.lastScrollJX = point["scrollM"]["jerkX"]
		self.lastScrollJY = point["scrollM"]["jerkY"]

		self.lastC0Hold =  point["buttons"]["0"]["avgClickHold"]
		self.lastC1Hold =  point["buttons"]["1"]["avgClickHold"]
		self.lastC2Hold =  point["buttons"]["2"]["avgClickHold"]
		try:
			self.lastC3Hold = point["buttons"]["3"]["avgClickHold"]
			self.lastC4Hold = point["buttons"]["4"]["avgClickHold"]
		except KeyError:
			self.lastC3Hold =  0
			self.lastC4Hold =  0


		self.lastClickPresh = point["avgClickPresh"]
		self.lastTouchPresh = point["avgTouchPresh"]
		self.lastPointerPresh = point["avgPointerPresh"]
		self.strikeSpeed = point["strikeSpeed"]
		self.strikeAcel = point["strikeAcel"]

		# Boolean Vars
		self.mobile |= point["mobile"]
		self.reload |= point["reload"]
		self.bckFwd |= point["bckFwd"]

		self.lastPackts[point["page"]] = point

	def toNumpyArray(self):
		#arr = np.array([self.totalTime, self.totalFS])
		a = []
		for key in self.__dict__.keys():
			if key == "mobile" or key == "reload" or key == "bckFwd":
				a.append(int(self.__dict__[key]))
			elif key != "key" and key != "lastDate" and key != "lastPackts" and key != "lp":
				a.append(self.__dict__[key])
		
		b = np.array(a, dtype=float)
		b[np.isnan(b)] = 0
		return b

	#When we save data to file we gon convert it to numpy arrays and store them in a csv file for l8r retrieval
