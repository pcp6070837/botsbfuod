import os
import json

def sortDir(lof):
	sof = []
	for filo in lof:
		if not (".json" in filo):
			continue
		if len(sof) == 0:
			sof.append(filo)
		elif len(sof) == 1:
			date = filo.split('-')[1]
			date = date.split('_')
			day = int(date[0])
			mon = int(date[1])
			d2 = sof[0].split('-')[1]
			d2 = d2.split('_')
			dd2 = int(d2[0])
			md2 = int(d2[1])

			if md2 < mon or (mon == md2 and dd2 > day):
				sof.insert(0, filo)
			else:
				sof.append(filo)
		else:
			date = filo.split('-')[1]
			date = date.split('_')
			day = int(date[0])
			mon = int(date[1])
			i = 0
			for i in range(0, len(sof)):
				d2 = sof[i].split('-')[1]
				d2 = d2.split('_')
				dd2 = int(d2[0])
				md2 = int(d2[1])
				if md2 > mon or (md2 == mon and dd2 > day):
					break
			sof.insert(i, filo)
	return sof

def loadJSONFromDirS(dir):
	data = []
	lof = os.listdir(dir)
	sof = sortDir(lof)

	for filo in sof:
		if ".json" in filo:
			f = open(dir + filo, "r")
			data += json.loads(f.read())
			
	return data

def loadJSONFromDir(dir):
	data = []
	lof = os.listdir(dir)
	for filo in lof:
		if ".json" in filo:
			f = open(dir + filo, "r")
			data += json.loads(f.read())
			
	return data