import json

flows = []

fcFile = open("seqcount1.json", "w")

flowFile = open("seq2.txt", "r")
flows = flowFile.readlines()

flowCounter = {}

for i in range(0, len(flows)):
	flow = flows[i].strip()
	if flow in flowCounter:
		continue
	else:
		count = 0
		for j in range(i, len(flows)):
			if flow in flows[j]:
				count += 1

		if count > 99:
			flowCounter[flow] = count

json.dump(flowCounter, fcFile)

