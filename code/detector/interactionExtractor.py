import re
import sys
import json
#Apontar só a sessões humanas, eliminar sessões com poucas interações
from itertools import groupby
from session import Session
from dataloader import loadJSONFromDir

reStr = "((C|c)rawler|(P|p)asser|Bot).*"
datasetDir = "d0/"
data = loadJSONFromDir(datasetDir)

c = {}
seshCount = 0

interSeqs = {}

sequenceFile = open("seq2.txt", "w")

for point in data:
	if re.search(reStr, point["ua"]):
		continue
	key = ""
	if "page" not in point:
		continue
	if "sid" in point:
		key = point["sid"]
	else:
		key = point["ip"] + point["ua"]

	if not (key in c):
		c[key] = Session(point)
		interSeqs[key] = "Nav:" + point["page"] + ", "
		seshCount += 1
	
	else:
		if (c[key].isSameSession(point)):
			actions = ""
			if (point["page"] != c[key].lp):
				actions += "Nav:" + point["page"] + " "
			else:
				if (point["mouseM"]["totalX"] != c[key].lastMouseX or point["mouseM"]["totalY"] != c[key].lastMouseY):
					actions += "MoveMouse "
				if (point["scrollM"]["totalX"] != c[key].lastScrollX or point["scrollM"]["totalY"] != c[key].lastScrollY):
					actions += "MoveScroll "
				if (point["touchM"]["totalX"] != c[key].lastTouchX or point["touchM"]["totalY"] != c[key].lastTouchY):
					actions += "MoveTouch " 

			lAs = c[key].totalAlphas

			lDs = c[key].totalDels

			lc0 = c[key].totalClick0 
			lc1 = c[key].totalClick1
			lc2 = c[key].totalClick2 
			c[key].addPoint(point)

			if (c[key].totalAlphas > lAs):
				actions += "Type "
			if (c[key].totalDels > lDs):
				actions += "DEL "
			
			if (lc0 > c[key].totalClick0):
				actions += "Click0 "
			if (lc1 > c[key].totalClick1):
				actions += "Click1 "
			if (lc2 > c[key].totalClick2):
				actions += "Click2 "
		
			if not (not actions) and actions != " ":
				actions += point["page"]
				al = actions.split(',')
				finalActs = [i[0] for i in groupby(al)]
				actions = ",".join(finalActs)
				interSeqs[key] += ", " + actions

		else:
			sesh = Session(point)
			seshCount += 1
			c[key] = sesh
			# pagesB4 = interSeqs[key].split(",")
			# pagesAft = []
			# for i in range(0, len(pagesB4) - 1):
			# 	if pagesB4[i] != pagesB4[i + 1]:
			# 		pagesAft.append(pagesB4[i])
					
			# pages = " -> ".join(pagesAft)
			
			#if not (not pages):
			tw = interSeqs[key].replace(", ,", ",")
			sequenceFile.write(tw + "\n")

			interSeqs[key] = "Nav:" + point["page"] + ", "