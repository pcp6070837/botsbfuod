import sys
import json
import joblib

from session import Session
from dataloader import loadJSONFromDir

datasetDir = "d4/"
data = loadJSONFromDir(datasetDir)

c = {}
seshCount = 0

counts = {}

clf = joblib.load("detector1k.pkl")

countFile = open("count.txt", "a")

for point in data:
	replaced = False
	key = ""
	if "page" not in point:
		point["page"] = ""
	if "sid" in point:
		key = point["sid"]
	else:
		key = point["ip"] + point["ua"]

	if not (key in c):
		c[key] = Session(point)
		seshCount += 1
	
	else:
		if (c[key].isSameSession(point)):
			c[key].addPoint(point)

		else:
			replaced = True
			sesh = Session(point)
			seshCount += 1
			c[key] = sesh
	
	if key not in counts:
		
		lastPak = False
		count = 0
		if (clf.predict(c[key].toNumpyArray().reshape(1,-1)) == -1):
			lastPak = True

		counts[key] = {"lp": lastPak, "c": count, "mc":count }

	else:
		pak = False
		if (clf.predict(c[key].toNumpyArray().reshape(1,-1)) == -1):
			pak = True

		if (counts[key]["lp"] and pak) and (not replaced):
			counts[key]["c"] += 1
			
			if (counts[key]["c"] > counts[key]["mc"]):
				counts[key]["mc"] = counts[key]["c"]
		
		elif (counts[key]["lp"] and not pak) and (not replaced):
			counts[key]["c"] = 0

		elif replaced:
			count = 0
			mac = counts[key]["mc"]
			counts[key] = {"lp": pak, "c": count, "mc":mac}
		
		counts[key]["lp"] = pak

json.dump(counts, countFile)