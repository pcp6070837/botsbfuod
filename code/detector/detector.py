import re
import sys
import joblib
import time
import numpy as np


from session import Session
from dataloader import loadJSONFromDir
from pyod.models.auto_encoder import AutoEncoder
from pyod.models.iforest import IForest
from pyod.models.lof import LOF
from sklearn.preprocessing import StandardScaler

reStr = ".*Bot.*"

reStr2 = "LinguineBot"

rng = np.random.RandomState(42)

X_humans = np.loadtxt("humans/humans.csv", delimiter=", ")
np.random.shuffle(X_humans)

X_humans = X_humans[0:522]
X_bots = np.loadtxt("crawlers/crawlers.csv", delimiter=", ")

# scaler = joblib.load("Standard.scaler")
# X_train = scaler.transform(X_train)
# X_humans = scaler.transform(X_humans)
# X_bots = scaler.transform(X_bots)

#joblib.dump(scaler, "Standard.scaler")

test = False

if ("test" in sys.argv and not "train" in sys.argv):
	test = True

train = False
if ("train" in sys.argv and not "test" in sys.argv):
	train = True

if train:

	print("Training")
	# Generate data
	X_train = np.loadtxt("d0/d0train.csv", delimiter=", ")
	# fit the model 69 estims got better as of n w/ max_samples = 2048 IsoForest 
	# , loss='binary_crossentropy'
	# for AE the best model so far has  hidden_neurons=[29, 14, 14, 29]
	clf = AutoEncoder(contamination=0.25, batch_size=2048, epochs=65, random_state=rng).fit(X_train)
	#clf = IForest(max_samples=2048, verbose=1, random_state=rng, n_estimators=65, n_jobs=-1, contamination=0.25).fit(X_train)
	#clf = LOF(contamination=0.25, n_jobs=-1).fit(X_train)
	joblib.dump(clf, "detectorAETest.pkl")
	print("Training Complete")
	print("Testing")
	total = 0
	pp = 0
	pn = 0
	tp = 0
	tn = 0
	fp = 0
	fn = 0

	y_pred_test = clf.predict(X_humans)
	for i in range(0, len(y_pred_test)):
		total += 1
		if y_pred_test[i] == 1:
			fp += 1
			pp += 1
		else:
			tn += 1
			pn += 1

	y_pred_test = clf.predict(X_bots)
	for i in range(0, len(y_pred_test)):
		total += 1
		if y_pred_test[i] == 1:
			tp += 1
			pp += 1
		else:
			fn += 1
			pn += 1

			
	print("Total: " + str(total))
	print("PP: " + str(pp))
	print("TP: " + str(tp))
	print("FP: " + str(fp))
	print("PN: " + str(pn))
	print("TN: " + str(tn))
	print("FN: " + str(fn))
	print("ACC: " + str((tp + tn)/total))
	print("F1: " + str((tp*2)/(2*tp + fp + fn)))


elif test:
	clf = joblib.load("detectorAE.pkl")

	total = 0
	pp = 0
	pn = 0
	tp = 0
	tn = 0
	fp = 0
	fn = 0

	y_pred_test = clf.predict(X_humans)
	for i in range(0, len(y_pred_test)):
		total += 1
		if y_pred_test[i] == 1:
			fp += 1
			pp += 1
		else:
			tn += 1
			pn += 1

	y_pred_test = clf.predict(X_bots)
	for i in range(0, len(y_pred_test)):
		total += 1
		if y_pred_test[i] == 1:
			tp += 1
			pp += 1
		else:
			fn += 1
			pn += 1

			
	print("Total: " + str(total))
	print("PP: " + str(pp))
	print("TP: " + str(tp))
	print("FP: " + str(fp))
	print("PN: " + str(pn))
	print("TN: " + str(tn))
	print("FN: " + str(fn))
	print("ACC: " + str((tp + tn)/total))
	print("F1: " + str((tp*2)/(2*tp + fp + fn)))