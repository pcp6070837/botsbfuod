import re
import sys
import json
from dataloader import loadJSONFromDirS

uaRe =  "^(SpagetBot|RavioliFunghiBot|PizzaBot|RavioliPestoBot|InterBot|InterBot2)$"
datasetDir = "dtest/"


lf = ""

if ("crawlers" in sys.argv):
	lf = "crawlers"
elif ("humans" in sys.argv):
	lf = "humans"
elif ("miss" in sys.argv):
	lf = "miss"


data = loadJSONFromDirS(datasetDir)
if lf == "crawlers":
	botData = []

	for point in data:
		if re.search(uaRe, point["ua"]):
			botData.append(point)

	df = open( datasetDir + "crawlers.json", "w")
	json.dump(botData, df, indent=4)

elif lf == "humans":
	idFile = open("ids.csv", "r")
	ids = idFile.read().split(",")
	idFile.close()
	usedIds = []
	hoomandata = []
	for point in data:
		if "sid" in point:
			for gid in ids:
				if point["sid"] == gid:
					hoomandata.append(point)
					if not gid in usedIds:
						usedIds.append(gid)
	print(usedIds)
	df = open( datasetDir + "humans.json", "w")
	json.dump(hoomandata, df, indent=4)

elif lf == "miss":
	missdata = []
	for point in data:
		if not ("page" in point):
			missdata.append(point)

	df = open(datasetDir + "miss.json", "w")
	json.dump(missdata, df, indent=4)