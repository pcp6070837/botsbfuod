# https://scikit-learn.org/stable/modules/generated/sklearn.metrics.RocCurveDisplay.html#sklearn.metrics.RocCurveDisplay.from_predictions

import joblib
import numpy as np
import matplotlib.pyplot as plt

from session import Session
from dataloader import loadJSONFromDir

from sklearn.metrics import f1_score
from sklearn.metrics import recall_score
from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_score
from sklearn.metrics import balanced_accuracy_score

from sklearn.metrics import RocCurveDisplay
from sklearn.metrics import ConfusionMatrixDisplay
from sklearn.metrics import PrecisionRecallDisplay

X_humans = np.loadtxt("humans/humans.csv", delimiter=", ")
np.random.shuffle(X_humans)
X_humans = X_humans[0:522]

X_bots = np.loadtxt("crawlers/crawlers.csv", delimiter=", ")

Y_humans = np.zeros(522)
Y_bots = np.ones(174)

X_total = np.concatenate((X_humans, X_bots))
Y_total = np.concatenate((Y_humans, Y_bots))

# scaler = joblib.load("Standard.scaler")
# X_total = scaler.transform(X_total)

#https://scikit-learn.org/stable/modules/generated/sklearn.metrics.confusion_matrix.html#sklearn.metrics.confusion_matrix

clf = joblib.load("detectorIF.pkl")

Y_test = clf.predict(X_total)
#print(Y_test[0:250])

print("Not accounting for class imbalance")
print("A:" + str(accuracy_score(Y_total, Y_test)))
print("P:" + str(precision_score(Y_total, Y_test)))
print("R:" + str(recall_score(Y_total, Y_test)))
print("F:" + str(f1_score(Y_total, Y_test)))

print("Accounting for class imbalance w/ minority of 25%")
print("B:" + str(balanced_accuracy_score(Y_total, Y_test)))
print("P:" + str(precision_score(Y_total, Y_test, average="macro")))
print("R:" + str(recall_score(Y_total, Y_test, average="macro")))
print("F:" + str(f1_score(Y_total, Y_test, average="macro")))


#Apresentar dados com e sem macro
#ConfusionMatrixDisplay.from_predictions([0, 1, 1, 0], [1, 0, 0, 1], cmap="binary")
RocCurveDisplay.from_predictions(Y_total, Y_test, name="LOF")
#ConfusionMatrixDisplay.from_predictions(Y_total, Y_test, cmap="binary")
#PrecisionRecallDisplay.from_predictions(Y_total, Y_test, name="IF")
plt.show()