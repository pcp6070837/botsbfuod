import sys
import time
import joblib

import numpy as np

from dateutil import parser
from session import Session
from dataloader import loadJSONFromDirS, loadJSONFromDir

c = {}


datasetDir = "humans/"

datacsv =  "humans/humansBalanced.csv"

data = loadJSONFromDir(datasetDir)


createDataSet = False
if ("create" in sys.argv):
	createDataSet = True

if createDataSet == True:
	seshCount = 0
	npArray = []
	for point in data:
		key = ""
		if "page" not in point:
			point["page"] = "" # this may happen cause of old browsers not having the proper location property.. can just skip these
			# we need this set or the detection fucks up.., but setting to jscrambler.com is okay too
		if "sid" in point:
			key = point["sid"]
		else:
			key = point["ip"] + point["ua"]

		if not (key in c):
			c[key] = Session(point)
			npArray.append(c[key].toNumpyArray())
			seshCount += 1

		else:
			if (c[key].isSameSession(point)):
				c[key].addPoint(point)
				npArray.append(c[key].toNumpyArray())

			else:
				sesh = Session(point)
				seshCount += 1
				c[key] = sesh
				npArray.append(c[key].toNumpyArray())
	
	np.random.shuffle(npArray)
	np.savetxt(datacsv, npArray, delimiter=", ")
	
	print("Session count: " + str(seshCount))

else:
	contents = np.loadtxt(datacsv, delimiter=", ")
	print(contents)