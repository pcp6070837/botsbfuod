import sys
import time
import joblib

import numpy as np

from dateutil import parser
from session import Session
from dataloader import loadJSONFromDirS, loadJSONFromDir

c = {}
d = {}
crawlerKS = []
found = []

clf = joblib.load("detectorAE.pkl")

datasetDir = "crawlers/"

data = loadJSONFromDir(datasetDir)

sc = 0
for point in data:
	key = ""
	if "page" not in point:
		point["page"] = "" # this may happen cause of old browsers not having the proper location property.. can just skip these
		# we need this set or the detection fucks up.., but setting to jscrambler.com is okay too
	if "sid" in point:
		key = point["sid"]
	else:
		key = point["ip"] + point["ua"]

	if not (key in c):
		crawlerKS.append(key)
		c[key] = Session(point)
		if clf.predict(c[key].toNumpyArray().reshape(1, -1)) == 1 and c[key].pc > 1:
			d[key] = c[key].pc
			found.append(point['ua'] + "PACKET COUNT: " + str(c[key].pc))

	else:
		if (c[key].isSameSession(point)):
			c[key].addPoint(point)
			if clf.predict(c[key].toNumpyArray().reshape(1, -1)) == 1 and not (key in d) and c[key].pc > 1:
				d[key] = c[key].pc
				found.append(point['ua'] + "PACKET COUNT: " + str(c[key].pc))

		else:
			sesh = Session(point)
			c[key] = sesh
			if clf.predict(c[key].toNumpyArray().reshape(1, -1)) == 1 and c[key].pc > 1:
				d[key] = c[key].pc
				found.append(point['ua'] + "PACKET COUNT: " + str(c[key].pc))

#print("Crawler Keys")
#print(c)

missed = []
for k in crawlerKS:
	if not (k in d):
		missed.append(k)

# joblib.dump(d, "correctBots.json")
# print("Regarding Bots")
# print("Missed Bot Seshes: " + str(len(missed)))
# print("Ideed Bot Seshes: " + str(len(d)))

c = {}
d = {}
crawlerKS = []

datasetDir = "humans/"

data = loadJSONFromDir(datasetDir)

for point in data:
	key = ""
	if "page" not in point:
		point["page"] = "" # this may happen cause of old browsers not having the proper location property.. can just skip these
		# we need this set or the detection fucks up.., but setting to jscrambler.com is okay too
	if "sid" in point:
		key = point["sid"]
	else:
		key = point["ip"] + point["ua"]

	if not (key in c):
		crawlerKS.append(key)
		c[key] = Session(point)
		if clf.predict(c[key].toNumpyArray().reshape(1, -1)) == 1 and c[key].pc > 1:
			d[key] = c[key].pc
			found.append(key + "PACKET COUNT: " + str(c[key].pc))

	else:
		if (c[key].isSameSession(point)):
			c[key].addPoint(point)
			if clf.predict(c[key].toNumpyArray().reshape(1, -1)) == 1  and (not (key in d) or d[key] > c[key].pc) and c[key].pc > 1:
				d[key] = c[key].pc
				found.append(key + "PACKET COUNT: " + str(c[key].pc))

		else:
			sesh = Session(point)
			if (key in d): 
				d.pop(key)
			c[key] = sesh
			if clf.predict(c[key].toNumpyArray().reshape(1, -1)) == 1 and c[key].pc > 1:
				d[key] = c[key].pc
				found.append(key + "PACKET COUNT: " + str(c[key].pc))

correct = []
for k in crawlerKS:
	if not (k in d):
		correct.append(k)

# print("Regarding Humans")
# print("Safe Human Seshs: " + str(len(correct)))
# print("Confused Human Seshs: " + str(len(d)))

#joblib.dump(d, "failedHumans.json")
print(found)
#print(len(found) - len(correct))